using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float HP { get; protected set; }
    protected float AttackAmount;
    protected float secretCode;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
           
    }

    /*public float GetHP()
    {
        return HP;
    }*/

    public void ApplyDamage(float dmg)
    {
        if (HP < 0)
            return;

        HP -= dmg;

    }

    protected virtual void AttackPattern(Transform target)
    {

    }
}
